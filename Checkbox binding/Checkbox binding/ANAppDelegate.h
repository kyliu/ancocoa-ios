//
//  ANAppDelegate.h
//  Checkbox binding
//
//  Created by Quincy Liu on 11-12-07.
//  Copyright (c) 2011 AbsoluteNoob All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ANAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *checkboxStatusLabel;
- (IBAction)checkCheckboxStatus:(NSButton *)sender;

@end
