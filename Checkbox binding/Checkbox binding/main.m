//
//  main.m
//  Checkbox binding
//
//  Created by Quincy Liu on 11-12-07.
//  Copyright (c) 2011 AbsoluteNoob All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
