//
//  ANAppDelegate.m
//  Checkbox binding
//
//  Created by Quincy Liu on 11-12-07.
//  Copyright (c) 2011 AbsoluteNoob All rights reserved.
//

#import "ANAppDelegate.h"

@implementation ANAppDelegate

@synthesize window = _window;
@synthesize checkboxStatusLabel = _checkboxStatusLabel;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    
    NSMutableDictionary * state = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"checkbox"];
    
    NSUserDefaults * preferences =[NSUserDefaults standardUserDefaults];
    
    [preferences registerDefaults:state];
    
    [self checkboxStatusLabel];
}

- (IBAction)checkCheckboxStatus:(NSButton *)sender {
    NSUserDefaults * preference = [NSUserDefaults standardUserDefaults];
    NSString * result;
    if ([preference boolForKey:@"checkbox"]) {
        result = @"checked";
    } else {
        result = @"unchecked";
    }
    [self.checkboxStatusLabel setStringValue:result];
}
@end
